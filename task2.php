<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>image</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>modal task</h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">click for  Modal</button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal</h4>
                </div>
                <div class="modal-body">



                        <form action="action.php" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="image">Image:</label>
                                <input type="file" class="form-control" name="image" placeholder="Add picture">
                            </div>
                            <div class="form-group">
                                <label for="text">Text:</label>
                                <textarea class="form-control" name="text" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="date">Pick a date:</label>
                                <input type="date" name="date">
                            </div>


                            <button type="submit" class="btn btn-primary">Submit</button>
</div>

               </div>

                </div>
         </div>

</div>

</body>
</html>